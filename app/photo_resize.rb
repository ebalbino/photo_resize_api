require 'mini_magick'
require 'net/http'
require 'json'
require 'fileutils'

PUBLIC_DIR = "public/images" 

def fetch_json(url)
  uri = URI(url)
  response = Net::HTTP.get_response(uri)

  case response
  when Net::HTTPSuccess then
    response.body
  else
    false
  end
end

def get_name_from_url(url)
  URI(url).path.split('/').last.split('.').first
end

def resize_image_and_write(image, size, output)
  image.resize size
  puts "Creating #{output}"
  image.write "#{output}"
end

def resize_image_from_url(url)
  image = MiniMagick::Image.open(url)
  image_name = get_name_from_url(url)
  image_dir = "#{PUBLIC_DIR}/#{image_name}"

  unless File.directory? PUBLIC_DIR
    FileUtils.mkdir_p PUBLIC_DIR
  end

  unless File.directory? image_dir
    puts "Creating #{image_dir}"
    Dir.mkdir image_dir
  end

  unless File.file? "#{image_dir}/big.jpg"
    resize_image_and_write(image, "640x480", "#{image_dir}/big.jpg")
  end

  unless File.file? "#{image_dir}/medium.jpg"
    resize_image_and_write(image, "384x288", "#{image_dir}/medium.jpg")
  end

  unless File.file? "#{image_dir}/small.jpg"
    resize_image_and_write(image, "320x240", "#{image_dir}/small.jpg")
  end
end

def consume_image_api_from_url(url)
  json_str = fetch_json(url)
  parsed = JSON.parse(json_str)
  names = []

  parsed["images"].each { |img|
    url = img["url"]
    puts "Resizing image: #{url}"
    resize_image_from_url(img["url"])
  }
end

