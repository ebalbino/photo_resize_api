class Photo
  include Mongoid::Document

  field :name, type: String
  field :small, type: String
  field :medium, type: String
  field :big, type: String

  validates :name, presence: true
  validates :small, presence: true
  validates :medium, presence: true
  validates :big, presence: true

  index({name: 1}, {unique: true, name: "name_index"})
end

def create_photo_from_name(name)
  small_url = "http://127.0.0.1:9292/images/#{name}/small.jpg"
  medium_url = "http://127.0.0.1:9292/images/#{name}/medium.jpg"
  big_url = "http://127.0.0.1:9292/images/#{name}/big.jpg"
  Photo.create(name: name, small: small_url, medium: medium_url, big: big_url)
end
