# require 'sinatra'
# require 'sinatra/namespace'
# require 'mongoid'

# Mongoid.load! "mongoid.yml"

get '/' do
  "Welcome to Photo Resize API"
end

namespace '/api/v1' do
  before do
    content_type 'application/json'
  end

  get '/photos' do
    Photo.all.to_json
  end
end

