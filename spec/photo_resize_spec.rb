require_relative './spec_helper.rb'

describe "Photo Resize Functions" do
  it "should return false if wrong address is provided" do
    expect(fetch_json('http://54.152.221.29/images.jso')).to eq false
  end

  it "should return name of path without extension" do
    expect(get_name_from_url('http://54.152.221.29/images/b777_2.jpg')).to eq 'b777_2'
  end

end
