require_relative './spec_helper.rb'

describe "Photo Resize API" do
  it "displays home page" do
    get '/'
    expect(last_response.body).to include("Welcome to Photo Resize API")
    expect(last_response.status).to eq 200
  end

  it "should return JSON array with 10 elements" do
    get '/api/v1/photos'
    expect(last_response.status).to eq 200
    expect(JSON.parse(last_response.body).length).to eq 10
  end

  it "should return element with fields: name, small, medium, big" do
    get '/api/v1/photos'
    expect(last_response.status).to eq 200
    expect(JSON.parse(last_response.body)[0]["name"]).not_to be_empty
    expect(JSON.parse(last_response.body)[0]["small"]).not_to be_empty
    expect(JSON.parse(last_response.body)[0]["medium"]).not_to be_empty
    expect(JSON.parse(last_response.body)[0]["big"]).not_to be_empty
  end

end
