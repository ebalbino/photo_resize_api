desc "Populates database"
task :populate do
  if File.directory? "public/images"
    directories = Dir.glob 'public/images/*'
    names = []
    directories.each { |directory|
      names << directory.split('/').last
    }
    names.each { |name|
      create_photo_from_name(name)
    }
    puts "Database populated."
  else
    puts "There are no images. Database can't be created. Please run 'rake consume_api' first."
  end
end
