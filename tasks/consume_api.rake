desc "Consume Photo API and generates images from links"
task :consume_api do
  consume_image_api_from_url('http://54.152.221.29/images.json')
  puts "API consumed and images created"
end
