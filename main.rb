require 'rubygems'
require 'bundler'

require 'sinatra'
require 'sinatra/namespace'
require 'mongoid'

require './app/photo'
require './app/photo_resize'
require './app/photo_api'

Mongoid.load! "mongoid.yml"

# (Dir["./app/*.rb"].sort).each do |file| 
#   require file
# end
