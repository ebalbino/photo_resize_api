# Photo Resize API

Consumes the endpoint ( http://54.152.221.29/images.json ) and creates 3 images of different sizes (640x480, 384x288, 320x240) from the images provided. These files are then served by the endpoint '/api/v1/photos'

## Dependencies

- ImageMagick (image processing)
- MongoDB (data storage)

## Getting started

```bash
$ git clone https://bitbucket.org/ebalbino/photo_resize_api
$ bundle install
$ bundle exec rackup config.ru
```

This series of commands downloads the repo, installs the gem dependencies and serves up the web application. At first, it doesn't serve anything 'api/v1/photos' since the API hasn't been consumed nor the database populated.

## Rake commands

```bash
$ bundle exec rake consume_api
$ bundle exec rake populate
```

The default rake command for the project runs the automated tests. Note that not all the tests will pass unless the database has been populated.

The application comes with two rake commands for consuming the API and populating the database. To populate the database, the API must first be consumed and processed by the application. 

The user will be notified if the database is being populated before the API has been consumed. 

If your local MongoDB server uses a different port than the default (27017), you can change the host in the file 'mongoid.yml' along with the database that you want to access. 

